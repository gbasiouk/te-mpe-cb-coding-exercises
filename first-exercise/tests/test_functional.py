import unittest
import json
from subprocess import run, PIPE

class TestApplicationFunctional(unittest.TestCase):
    """
    Functional tests for the application.
    These tests simulate running the program from the command line.
    """
    
    def test_functional(self):
        result = run(['python', 'list_utils.py', '--find-duplicates', '["a", "b", "a"]'], stdout=PIPE, text=True)
        parsed_output = json.loads(result.stdout)
        duplicates = parsed_output["Duplicates"]
        self.assertEqual(duplicates, ['a'])

if __name__ == '__main__':
    unittest.main()