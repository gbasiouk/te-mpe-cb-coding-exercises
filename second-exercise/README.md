# Second Exercise

## Overview
The Dependency Graph Resolver is a Python-based tool designed for analyzing and displaying package dependencies. It reads a JSON file specifying packages and their dependencies, then constructs and visualizes a complete dependency graph. This tool is useful for understanding complex dependency structures in software projects.

## Prerequisites
Python: Version 3.6+

## Usage
Reading and Resolving Dependency Graph.
To resolve and print the dependency graph, place a JSON file (e.g., deps.json) in a fixed filesystem location (e.g., /tmp/deps.json). The JSON file should follow this format:

```
{
  "pkg1": ["pkg2", "pkg3"],
  "pkg2": ["pkg3"],
  "pkg3": []
}
```

### Run the program using the command:

```python -m python -m dependency_graph.py <path_to_json_file>```

## Output Format
The output will be a visual representation of the full dependency graph, displayed in a nested format:

```
- pkg1
  - pkg2
    - pkg3
  - pkg3
- pkg2
  - pkg3
- pkg3
```

## Testing
Ensure you are in the second-exercise directory.

### Running Tests
Execute the following command to run all tests:

```python -m unittest discover -s tests```

### Types of Tests
**Unit Tests:** Verify the individual functions and components, ensuring that each part performs as expected.

**Integration Tests:** Evaluate the combined operation of multiple components to ensure they work together correctly.

**Edge Cases:** Special focus on edge cases like empty dependency lists and cyclic dependencies (if applicable).
