import unittest
from list_utils.core import find_duplicates

class TestFindDuplicates(unittest.TestCase):
    """
    Test cases for find_duplicates function.
    """
    
    def test_empty_list(self):
        self.assertEqual(find_duplicates([]), [])

    def test_no_duplicates(self):
        self.assertEqual(find_duplicates(['a', 'b', 'c']), [])

    def test_with_duplicates(self):
        self.assertEqual(find_duplicates(['a', 'b', 'a', 'c', 'b']), ['a', 'b'])
    
    def test_with_duplicates_ordered(self):
        self.assertEqual(find_duplicates(['b', 'b', 'a', 'c', 'b', 'a']), ['b', 'a'])
    
    def test_with_full_strings(self):
        self.assertEqual(find_duplicates(["apple", "banana", "kiwi", "apple", "orange", "banana"]), ["apple", "banana"])
    
    def test_with_numbers(self):
        self.assertEqual(find_duplicates([1, 2, 3, 1, 4, 2]), [1, 2])

if __name__ == '__main__':
    unittest.main()