import sys
from dependency_graph.graph import resolve_dependencies

def main():
    """Main function to run the dependency graph program."""
    
    if len(sys.argv) != 2:
        print("Usage: python dependency_graph.py <path_to_json_file>")
        sys.exit(1)

    filename = sys.argv[1]
    graph = resolve_dependencies(filename)
    print(graph)

if __name__ == "__main__":
    main()