import json
import unittest
from unittest.mock import patch, mock_open, MagicMock
from dependency_graph.graph import resolve_dependencies, load_dependencies, construct_graph

class TestGraph(unittest.TestCase):

    def test_load_dependencies_with_valid_data(self):
        mock_data = json.dumps({
            "pkg1": ["pkg2", "pkg3"],
            "pkg2": ["pkg3"],
            "pkg3": []
        })

        with patch("builtins.open", mock_open(read_data=mock_data)):
            data = load_dependencies("dummy/path.json")
            self.assertEqual(len(data), 3)

    def test_load_dependencies_with_empty_data(self):
        with patch("builtins.open", mock_open(read_data="{}")):
            data = load_dependencies("dummy/path.json")
            self.assertEqual(data, {})

    def test_resolve_dependencies_with_valid_data(self):
        mocked_data = {
            "pkg1": ["pkg2", "pkg3"],
            "pkg2": ["pkg3"],
            "pkg3": []
        }
        
        with patch('dependency_graph.graph.load_dependencies', return_value=mocked_data):
            graph_str = resolve_dependencies("dummy/path")

        expected_output = (
            "- pkg1\n"
            "  - pkg2\n"
            "    - pkg3\n"
            "  - pkg3\n"
            "- pkg2\n"
            "  - pkg3\n"
            "- pkg3"
        )

        self.assertEqual(graph_str, expected_output)
    
    def test_construct_graph_with_cyclic_dependency(self):
        dependencies = {
            "pkg1": ["pkg2"],
            "pkg2": ["pkg1"]
        }

        with self.assertRaises(ValueError) as context:
            construct_graph(dependencies, "pkg1", {}, set())
        self.assertIn("Cyclic dependency detected", str(context.exception))

    def test_construct_graph_with_empty_dependency_list(self):
        dependencies = {
            "pkg1": []
        }

        resolved = {}
        construct_graph(dependencies, "pkg1", resolved, set())
        self.assertEqual(resolved, {"pkg1": {}})

    def test_load_dependencies_with_missing_file(self):
        with patch("builtins.open", MagicMock(side_effect=FileNotFoundError())):
            with self.assertRaises(FileNotFoundError):
                load_dependencies("nonexistent_file.json")