import unittest
from list_utils.cli import parse_args
from list_utils.core import find_duplicates

class TestCLIIntegration(unittest.TestCase):
    """
    Integration tests for the Command Line Interface (CLI).
    These tests ensure that different components of the application work together correctly.
    """
    
    def test_integration(self):
        args = parse_args(['--find-duplicates', '["a", "b", "a", "c", "b"]'])
        self.assertEqual(find_duplicates(args.find_duplicates), ['a', 'b'])

if __name__ == '__main__':
    unittest.main()