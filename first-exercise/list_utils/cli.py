import argparse
import json

def parse_args(args=None):
    """
    Parses command-line arguments.

    Returns:
    argparse.Namespace: The parsed arguments.
    """
    parser = argparse.ArgumentParser(description="List utilities")
    parser.add_argument('--find-duplicates', type=json.loads, help="Find duplicates in a list")
    return parser.parse_args(args=args)