import json
from typing import Dict, List, Any

def load_dependencies(filename: str) -> Dict[str, List[str]]:
    """Load dependencies from a JSON file.

    Args:
        filename (str): Path to the JSON file containing dependencies.

    Returns:
        Dict[str, List[str]]: A dictionary representing the dependency graph.
    """
    with open(filename, 'r') as file:
        return json.load(file)

def construct_graph(dependencies: Dict[str, List[str]], current_pkg: str, 
                    resolved: Dict[str, Any], seen: set) -> None:
    """Recursively construct the dependency graph.
    
    Args:
        dependencies (Dict[str, List[str]]): All package dependencies.
        current_pkg (str): The current package being resolved.
        resolved (Dict[str, Any]): The resolved graph so far.
        seen (set): Packages that have been seen to detect cycles.
    """
    if current_pkg in seen:
        raise ValueError(f"Cyclic dependency detected at {current_pkg}")

    seen.add(current_pkg)
    resolved[current_pkg] = {}

    for dep in dependencies.get(current_pkg, []):
        if dep not in resolved:
            construct_graph(dependencies, dep, resolved[current_pkg], seen.copy())

def format_graph(graph: Dict[str, Any], indent: int = 0) -> str:
    """Format the dependency graph into a string with indentation.

    Args:
        graph (Dict[str, Any]): The resolved dependency graph.
        indent (int): Current indentation level.

    Returns:
        str: Formatted graph as a string.
    """
    output = []
    for node, children in graph.items():
        output.append("  " * indent + "- " + node)
        if children:
            output.append(format_graph(children, indent + 1))
    return "\n".join(output)

def resolve_dependencies(filename: str) -> str:
    """Resolve dependencies from a given file and construct the formatted dependency graph."""
    dependencies = load_dependencies(filename)
    resolved_graph = {}

    for pkg in dependencies.keys():
        if pkg not in resolved_graph:
            construct_graph(dependencies, pkg, resolved_graph, set())

    return format_graph(resolved_graph)