from typing import List, TypeVar

T = TypeVar('T')

def find_duplicates(input_list: List[T]) -> List[T]:
    """
    Finds all duplicates elements in a list and returns them in the order of first appearance.

    Args:
    input_list (List[T]): The list of elements to check for duplicates.

    Returns:
    List[T]: A list of duplicated elements in the order of first appearance.
    """
    seen = set()
    unique = set()
    duplicates = []

    # First pass to identify all unique elements and seen elements
    for item in input_list:
        if item in seen:
            unique.add(item)
        else:
            seen.add(item)

    # Second pass to maintain the order of first appearances
    for item in input_list:
        if item in unique:
            duplicates.append(item)
            unique.remove(item)

    return duplicates