import json
from list_utils.cli import parse_args
from list_utils.core import find_duplicates

if __name__ == "__main__":
    args = parse_args()
    
    if args.find_duplicates:
        duplicates = find_duplicates(args.find_duplicates)
        output = {
            "Input List": args.find_duplicates,
            "Duplicates": duplicates
        }
        print(json.dumps(output))  
    else:
        print("No command provided. Use --help for usage instructions.")