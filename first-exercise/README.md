# First Exercise

## Overview
The List Utilities project provides a command-line interface (CLI) for operations on lists, including finding duplicates. It's designed for ease of use, efficiency, and extensibility.

## Prerequisites
Python: Version 3.7+

## Usage
Finding Duplicates in a List

```
python list_utils.py --find-duplicates '["a", "a", "b"]'
```

## Testing
Ensure you are in the first-exercise directory.

Execute the following command to run all tests:


```python -m unittest discover tests```

### Types of Tests
**Unit Tests:** Validate individual components for correctness.

**Integration Tests:** Check the interactions between components.

**Functional Tests:** Assess the application from the user's perspective.
